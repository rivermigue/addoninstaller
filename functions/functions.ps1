# Vars
$Utf8NoBomEncoding = New-Object System.Text.UTF8Encoding $False

function Validate-Paths
{
	foreach ($dir in $directories)
	{
		if (Test-Path -Path "$dir") {
			Write-Host "Directorio $dir existe, continuando con ejecucion" -ForegroundColor DarkGreen
		} else {
			Write-Host "Directorio $dir no existe, saliendo del script" -ForegroundColor red
			return
		}
	}
}

function Install-Behaviors
{
	if (!$behaviors) {
		Write-Host "Warning: No se ha encontrado ningun behavior" -ForegroundColor yellow
	} else {
		Write-Host "Los siguientes behaviors han sido encontrados" -ForegroundColor yellow
		foreach ($behavior in $behaviors)
		{
			if (Test-Path -Path "$behaviors_dir\$behavior\manifest.json" -PathType leaf) {
				$behavior_name = Get-Content -Raw -Path "$behaviors_dir\$behavior\manifest.json" | ConvertFrom-Json
				Write-Host $behavior_name[0].header.name -ForegroundColor Green
				$data1 += @(
					@{
						pack_id	= $behavior_name[0].header.uuid
						version	= $behavior_name[0].header.version
					}
				 )
			} else {
				Write-Host "Parece que --> $behavior <-- no es un behavior pack valido y no sera incluido en la instalacion" -ForegroundColor red
			}
		}
		if (Test-Path -Path "$worlds_dir\world") {
			Write-Host "Instalando los behavior(s) en $worlds_dir\world"
			$output1 = Write-Output $data1 | ConvertTo-JSON
			[System.IO.File]::WriteAllLines("$worlds_dir\world\world_behavior_packs.json", $output1, $Utf8NoBomEncoding)
			Write-Host "Instalacion completada con exito!" -ForegroundColor black -BackgroundColor White
		} else {
			Write-Host "ERROR: El directorio $worlds_dir\world no existe, porfavor crealo e importa tu mundo actual en el y vuelve a correr el script" -ForegroundColor red
			return
		}
	}
}

function Install-Resources
{
	if (!$resources) {
		Write-Host "Warning: No se ha encontrado ningun resource" -ForegroundColor yellow
	} else {
		Write-Host "Los siguientes resources han sido encontrados" -ForegroundColor yellow
		foreach ($resource in $resources)
		{
			if (Test-Path -Path "$resources_dir\$resource\manifest.json" -PathType leaf) {
				$resource_name = Get-Content -Raw -Path "$resources_dir\$resource\manifest.json" | ConvertFrom-Json
				Write-Host $resource_name[0].header.name -ForegroundColor Green
				$data2 += @(
					@{
						pack_id	= $resource_name[0].header.uuid
						version	= $resource_name[0].header.version
					}
				 )
			} else {
				Write-Host "Parece que --> $resource <-- no es un resource pack valido y no sera incluido en la instalacion" -ForegroundColor red
			}
		}
		if (Test-Path -Path "$worlds_dir\world") {
			Write-Host "Instalando los resource(s) en $worlds_dir\world"
			$output2 = Write-Output $data2 | ConvertTo-JSON
			[System.IO.File]::WriteAllLines("$worlds_dir\world\world_resource_packs.json", $output2, $Utf8NoBomEncoding)
			Write-Host "Instalacion completada con exito!" -ForegroundColor black -BackgroundColor White
		} else {
			Write-Host "ERROR: El directorio $worlds_dir\world no existe, porfavor crealo e importa tu mundo actual en el y vuelve a correr el script" -ForegroundColor red
			return
		}
	}
}