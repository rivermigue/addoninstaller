# Importing functions
. "$PSScriptRoot\functions\functions.ps1"

# Vars
$behaviors_dir = "$PSScriptRoot\mods\behaviors"
$resources_dir = "$PSScriptRoot\mods\resources"
$worlds_dir    = "$PSScriptRoot\worlds"
[array]$directories = "$behaviors_dir", "$resources_dir", "$worlds_dir"
[array]$behaviors = Get-ChildItem $behaviors_dir -Directory | Foreach-Object {$_.Name}
[array]$resources = Get-ChildItem $resources_dir -Directory | Foreach-Object {$_.Name}

function Show-Menu
{
    param (
          [string]$Title = 'Menu Principal'
    )
    cls
    Write-Host "================ $Title ================"
	Write-Host "1: Presiona '1' para instalar resources y behaviors en un mundo existent."
    Write-Host "2: Presiona '2' para crear un zip de los recursos, behaviors y el mundo."
    Write-Host "3: Presiona '3' para transferir los recursos por FTP al servidor."
    Write-Host "Q: Presiona 'Q' para salir."
}

do
{
    Show-Menu
    $input = Read-Host -Prompt "Selecciona una opcion"
    if ($input -eq '1') {
        cls
        Write-Host "Instalacion de resources y behaviors"
        Write-Host "1: Presiona '1' para instalar behaviors"
        Write-Host "2: Presiona '2' para instalar resources"
        Write-Host "B: Presiona 'B' para regresar al menu anterior"
        $input2 = Read-Host -Prompt "Selecciona una opcion"
        do
        {
            if ($input2 -eq '1') {
                Validate-Paths
                Install-Behaviors
                return
            } elseif ($input2 -eq '2') {
                Validate-Paths
                Install-Resources
                return
            } elseif ($input2 -eq 'b') {
                Write-Host "Volviendo al menu principal" -ForegroundColor black -BackgroundColor White
            } else {
                Write-Host "No se ha seleccionado una opcion valida, abortando ejecucion"
                return
            }
        } until ($input2 -eq 'b')
    } elseif ($input -eq '2') {
        Write-Host "Escogiste opcion 2"
		Write-Host "Volviendo al menu principal" -ForegroundColor yellow
    } elseif ($input -eq '3') {
        Write-Host "Escogiste opcion 3"
		Write-Host "Volviendo al menu principal" -ForegroundColor yellow
    } elseif ($input -eq 'q') {
        Write-Host "Saliendo del wizzard" -ForegroundColor black -BackgroundColor White
        return
    } else {
        cls
        Write-Host "No se ha seleccionado una opcion valida, porfavor escoge una opcion listada dentro del menu"
    }
    pause
}
until ($input -eq 'q')